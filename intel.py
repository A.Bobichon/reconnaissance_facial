import numpy as np

class Flower():
    def __init__(self, width, height, color=None):
        self.color = color
        self.width = width
        self.height = height

class neural_network(object):
    def __init__(self):
        self.inputSize = 2
        self.outputSize = 1
        self.hiddenSize = 3

        self.w1 = np.random.randn(self.inputSize, self.hiddenSize)
        self.w2 = np.random.randn(self.inputSize, self.hiddenSize)

    def sigmoid(self, s):
        return 1/(1+np.exp(-s))

    def sigmoid_prime(self,s):
        return s * (1-s)

    def forward(self, x):
        self.z = np.dot(x, self.w1)
        self.z2 = self.sigmoid(self.z)
        self.z3 = np.dot(self.z2, self.w2)
        o = self.sigmoid(self.z3)
        return o
        
    def backward(self, x, y, o):
        
        self.o_error = y - o
        self.delta = self.o_error * self.sigmoid_prime(o)

        self.z2_error = self.o_delta.dot(self.w2.T)
        self.z2_delta = self.z2_error * self.sigmoid_prime(self.z2)

        self.w1 += x.T.dot(self.w2.T)
        self.w2 += self.z2.T.dot(self.o_delta)

    def train(self, x, y):
        o = self.forward(x)
        self.backward(x,y,o)

x_entry = np.array(
    (  
        [3,1.5], [2,1], [4,1.5],  
        [3,1], [3.5,0.5], [2,0.5],
        [5.5,1], [1,1], [4.5,1] 
    ), dtype=float
)

y = np.array(
    (
        [1],[0],[1],
        [0],[1],[0],
        [1],[0]
    ), dtype=float 
)


x_entry = x_entry/np.amax(x_entry, axis=0)

x = np.split(x_entry,[8])[0]
x_prediction = np.split(x_entry,[8])[1]

NN = neural_network()
o = NN.forward(x)


    